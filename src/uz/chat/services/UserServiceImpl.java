package uz.chat.services;

import uz.chat.Store;
import uz.chat.model.User;
import uz.chat.services.interfaceSer.UserService;

import static uz.chat.Store.*;


public class UserServiceImpl implements UserService {

    static MessageServiceImpl messageService = new MessageServiceImpl();
    static GroupServiceImpl groupService = new GroupServiceImpl();

    @Override
    public void login() {

        System.out.println("Enter email");
        String email = scannerStr.nextLine();
        System.out.println("Enter password");
        String password = scannerStr.nextLine();

        User loginUser = Store.userList.stream()
                .filter(user -> user.getEmail().equals(email) && user.getPassword().equals(password))
                .findFirst().orElse(null);

        if (loginUser != null) {

            while (true) {
                System.out.println("1=>Personal. 2=>Users. 3=>SendMessage  4=>My Groups 5=>Groups 0=>Logout");
                int option = Store.scannerInt.nextInt();
                switch (option) {
                    case 1:
                        messageService.peronals(loginUser);
                        break;
                    case 2:
                        users(loginUser);
                        break;
                    case 3:
                        messageService.sendMessage(loginUser);
                        break;
                    case 4:
                        groupService.myGroups(loginUser);
                        break;
                    case 5:
                        groupService.groups(loginUser);
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Wrong option");
                        break;
                }
            }

        } else {
            System.out.println("User not found");
        }


    }

    @Override
    public void users(User loginUser) {
        userList.stream().
                filter(user -> !user.equals(loginUser))
                .forEach(System.out::println);
    }
}
