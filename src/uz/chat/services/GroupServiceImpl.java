package uz.chat.services;

import uz.chat.model.Group;
import uz.chat.model.User;
import uz.chat.services.interfaceSer.GroupService;

import static uz.chat.Store.groupList;
import static uz.chat.Store.scannerInt;

public class GroupServiceImpl implements GroupService {

    static UserServiceImpl userService = new UserServiceImpl();
    static MessageServiceImpl messageService = new MessageServiceImpl();


    @Override
    public void addUser(User user, Group group) {
        userService.users(user);
        User selectedUser = messageService.select();
        if (selectedUser != null) {
            boolean res = group.addMember(group, selectedUser);
            if (res) System.out.println("Successfully added");

        } else {
            System.out.println("Not found");
        }
    }

    @Override
    public void groups(User user) {
        System.out.println("Groups added me by someone");

        for (Group group : groupList) {
            if (group.getMembers() != null) {
                if (!group.getAdmin().equals(user)) {
                    for (User member : group.getMembers()) {
                        if (member.equals(user)) {
                            System.out.println("id=>" + group.getId() + ", name=>" + group.getName());
                        }
                    }
                }
            }
        }
        System.out.println("Enter group  id");
        int groupId = scannerInt.nextInt();
        Group selectGr = groupList.stream()
                .filter(group -> group.getId() == groupId)
                .findFirst().orElse(null);
        if (selectGr != null) {
            System.out.println("1=>SendMesssage. 2=>UserList");
            int option = scannerInt.nextInt();
            switch (option) {
                case 1:
                    messageService.sendMessage(user, selectGr);
                    break;
                case 2:
                    if (selectGr.getMembers() != null) {
                        selectGr.getMembers().stream()
                                .forEach(member -> System.out.println("id=>" + member.getId() + ", name=>" + member.getName()));
                    }
                    break;

            }
        }


    }

    private void groupShowMem(User user) {
        groupList.stream()
                .filter(group -> group.getAdmin().equals(user))
                .forEach(group -> System.out.println("id=>" + group.getId() + ", name=>" + group.getName()));

        System.out.println("Enter group  id to seeMembers");
        int groupId = scannerInt.nextInt();

        Group selectedGroup = groupList.stream()
                .filter(group -> group.getAdmin().equals(user) && group.getId() == groupId)
                .findFirst().orElse(null);

        if (selectedGroup != null) {
            groupUsers(user, selectedGroup);
        } else {
            System.out.println("Not found");
        }
    }

    public void myGroups(User user) {
        System.out.println("MyGroups");
        groupList.stream()
                .filter(group -> group.getAdmin().equals(user))
                .forEach(group -> System.out.println("id=>" + group.getId() + ", name=>" + group.getName()));
        System.out.println("1=>Add user. 2=>User List . 3=>SendMessage");
        int opt = scannerInt.nextInt();
        switch (opt) {
            case 1: {
                groupList.stream()
                        .filter(group -> group.getAdmin().equals(user))
                        .forEach(group -> System.out.println("id=>" + group.getId() + ", name=>" + group.getName()));
                Group selectedGroup = selectedGroup();

                if (selectedGroup != null) {
                    addUser(user, selectedGroup);
                } else {
                    System.out.println("Not found");
                }
                break;
            }
            case 2:
                groupShowMem(user);
                break;
            case 3: {
                groupList.stream()
                        .filter(group -> group.getAdmin().equals(user))
                        .forEach(group -> System.out.println("id=>" + group.getId() + ", name=>" + group.getName()));
                Group selectedGroup = selectedGroup();
                if (selectedGroup != null) {
                    messageService.sendMessage(user, selectedGroup);
                } else {
                    System.out.println("Not found");
                }
                break;
            }
        }


    }

    public void groupUsers(User user, Group group) {
        if (group.getMembers() != null) {
            for (User member : group.getMembers()) {
                System.out.println("id=>" + member.getId() + ", name=>" + member.getName());
            }
        }
    }

    public Group selectedGroup() {
        System.out.println("Enter group  id to seeMembers");
        int groupId = scannerInt.nextInt();
        return groupList.stream()
                .filter(group -> group.getId() == groupId)
                .findFirst().orElse(null);
    }


}
