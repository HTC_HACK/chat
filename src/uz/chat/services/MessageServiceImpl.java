package uz.chat.services;

import uz.chat.model.Group;
import uz.chat.model.Message;
import uz.chat.model.User;
import uz.chat.model.enums.MessageStatus;
import uz.chat.services.interfaceSer.MessageService;

import static uz.chat.Store.*;

public class MessageServiceImpl implements MessageService {

    static UserServiceImpl userService = new UserServiceImpl();

    @Override
    public void sendMessage(User user) {
        userService.users(user);
        User selectedUser = select();

        if (selectedUser != null) {
            System.out.println("Enter message");
            String body = scannerStr.nextLine();

            Message message = new Message((int) (Math.random() * 10000), body, user, selectedUser, MessageStatus.UNREAD);
            messageList.add(message);

        } else {
            System.out.println("Not found");
        }

    }

    public void sendMessage(User user, Group group) {
        for (Message message : messageList) {
            if (message.getGroupReceiver().equals(group)) {
                if (!message.getSender().equals(user)) {
                    System.out.println(message.getSender().getName() + "=>" + message.getBody());
                } else if (message.getSender().equals(user)) {
                    System.out.println("\t\t\t\t\t" + message.getBody());
                }
            }
        }
        System.out.println("Enter message");
        String body = scannerStr.nextLine();

        Message message = new Message((int) (Math.random() * 10000), body, user, group, MessageStatus.UNREAD);
        messageList.add(message);
    }

    @Override
    public void peronals(User loginUser) {

        System.out.println("Personals");
        int count = 0;
        for (User user1 : userList) {

            if (!user1.equals(loginUser)) {
                for (Message message : messageList) {
                    if (message.getSender().equals(loginUser) && message.getReceiver().equals(user1)
                            || message.getSender().equals(user1) && message.getReceiver().equals(loginUser)
                    ) {
                        System.out.println("name=>" + user1.getName() + ", id=>" + user1.getId() +
                                ", new message=>(" + countUnread(user1, loginUser) + ")");
                        count++;
                        break;
                    }
                }
            }

        }
        if (count == 0) {
            System.out.println("No personal");
            return;
        }
        User selectedUser = select();
        if (selectedUser != null) {

            for (Message message : messageList) {
                if (message.getSender().equals(loginUser) && message.getReceiver().equals(selectedUser)) {
                    message.setStatus(MessageStatus.READ);
                    System.out.println("\t\t\t\t" + message.getBody());
                    System.out.println("\t\t\t\t----------------");
                } else if (message.getSender().equals(selectedUser) && message.getReceiver().equals(loginUser)) {
                    message.setStatus(MessageStatus.READ);
                    System.out.println(message.getBody());
                    System.out.println("----------------");
                }
            }
            System.out.println("Enter message 0=>back");
            String body = scannerStr.nextLine();
            if (body.equals("0")) return;
            Message message = new Message((int) (Math.random() * 10000), body, loginUser, selectedUser, MessageStatus.UNREAD);
            messageList.add(message);

        } else {
            System.out.println("Not found");
        }


    }

    private int countUnread(User selectedUser, User loginUser) {
        int count = 0;

        for (Message message : messageList) {
            if (message.getSender().equals(selectedUser) && message.getReceiver().equals(loginUser)
                    && message.getStatus().equals(MessageStatus.UNREAD)) {
                count++;
            }
        }

        return count;
    }

    public User select() {
        System.out.println("Enter user id");
        int userId = scannerInt.nextInt();

        return userList.stream()
                .filter(user1 -> user1.getId() == userId)
                .findFirst().orElse(null);
    }
}
