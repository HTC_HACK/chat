package uz.chat.model.enums;

public enum MessageStatus {

    READ,
    UNREAD
}
