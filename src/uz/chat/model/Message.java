package uz.chat.model;

import uz.chat.model.enums.MessageStatus;

public class Message {

    private Integer id;
    private String body;
    private User sender;
    private User receiver;
    private Group groupReceiver;
    private MessageStatus status = MessageStatus.UNREAD;

    public Message() {
    }

    public Message(Integer id, String body, User sender, Group groupReceiver, MessageStatus status) {
        this.id = id;
        this.body = body;
        this.sender = sender;
        this.groupReceiver = groupReceiver;
        this.status = status;
    }

    public Message(Integer id, String body, User sender, User receiver, MessageStatus status) {
        this.id = id;
        this.body = body;
        this.sender = sender;
        this.receiver = receiver;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Group getGroupReceiver() {
        return groupReceiver;
    }

    public void setGroupReceiver(Group groupReceiver) {
        this.groupReceiver = groupReceiver;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", groupReceiver=" + groupReceiver +
                ", status=" + status +
                '}';
    }
}
