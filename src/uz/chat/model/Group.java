package uz.chat.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private Integer id;
    private String name;
    private User admin;
    private List<User> members = new ArrayList<>();

    public Boolean addMember(Group group, User user) {
        User user1 = group.members.stream()
                .filter(group1 -> group1.equals(user))
                .findFirst().orElse(null);
        if (user1 != null) {
            System.out.println("Already Exist");
            return false;
        }

        group.members.add(user);
        return true;
    }


    public Group() {
    }

    public Group(Integer id, String name, User admin) {
        this.id = id;
        this.name = name;
        this.admin = admin;
    }

    public Group(Integer id, String name, User admin, List<User> members) {
        this.id = id;
        this.name = name;
        this.admin = admin;
        this.members = members;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", admin=" + admin +
                ", members=" + members +
                '}';
    }
}
