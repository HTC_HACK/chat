package uz.chat;

import uz.chat.model.Group;
import uz.chat.model.Message;
import uz.chat.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Store {

    public static Scanner scannerInt = new Scanner(System.in);
    public static Scanner scannerStr = new Scanner(System.in);
    public static Scanner scannerDou = new Scanner(System.in);
    static User user1 = new User(1, "admin", "1", "1345");
    static User user2 = new User(3, "bekzod", "3", "1345");
    public static List<User> userList = new ArrayList<>(
            Arrays.asList(
                    user1,
                    new User(2, "eldor", "2", "1345"),
                    user2,
                    new User(4, "asadbek", "4", "1345")
            )
    );

    public static List<Message> messageList = new ArrayList<>();
    public static List<Group> groupList = new ArrayList<>(
            Arrays.asList(
                    new Group(1, "B7", user1),
                    new Group(2, "B6", user2)
            )
    );



}
